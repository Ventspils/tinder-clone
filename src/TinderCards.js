import React, { useState } from 'react';
import TinderCard from 'react-tinder-card';
import './TinderCards.css';

function TinderCards() {
  const [people, setPeople] = useState([
    {
      name: 'Елена (29)',
      url:
        'https://ae01.alicdn.com/kf/HTB1tj3BLVzqK1RjSZFoq6zfcXXaf/Hot-Sexy-Girl-Photo-SILK-POSTER-Decorative-Wall-painting-24x36inch.jpg_Q90.jpg_.webp',
    },
    {
      name: 'Света (28)',
      url:
        'https://ae01.alicdn.com/kf/HTB1mB2BQFXXXXcfXVXXq6xXFXXX8/Full-Swimsuit-Stripe-Open-Hot-Sexy-Girl-Swimwear-Summer-Biki-M17072.jpg',
    },
  ]);

  const swiped = (direction, nameToDelete) => {
    console.log('removing: ' + nameToDelete);
    // setLastDirection(direction);
  };

  const outOfFrame = (name) => {
    console.log(name + ' left the screen!');
  };

  return (
    <div className="tinderCards">
      <div className="tinderCards__cardContainer">
        {people.map((person) => (
          <TinderCard
            className="swipe"
            key={person.name}
            preventSwipe={['up', 'down']}
            onSwipe={(dir) => swiped(dir, person.name)}
            onCardLeftScreen={() => outOfFrame(person.name)}
          >
            <div
              style={{ backgroundImage: `url(${person.url})` }}
              className="card"
            >
              <h3>{person.name}</h3>
            </div>
          </TinderCard>
        ))}
      </div>
    </div>
  );
}

export default TinderCards;
